from turtle import *
#Grundeinstellungen
pencolor("red")
fillcolor("red")
hideturtle()
speed(10)
#Die Länge wird als Variable l bezeichnet und vom User definiert
l=int(numinput("Länge","Bitte geben Sie die Länge ein"))
#l2-l4 werden definiert, damit man nicht jedes mal int(l/2) schreiben muss.
l2=int(l/2)
l3=int(l/5)
l4=int(l/10)

#Die Turtle wird in Position gebracht, sodass die Flagge am Ende zentriert ist
penup()
right(90)
forward(l2)
right(90)
pendown()

#Das Quadrat wird gezeichnet
begin_fill()
forward(l2)
right(90)
forward(l)
right(90)
forward(l)
right(90)
forward(l)
right(90)
forward(l2)
end_fill()

#Die Turtle wird in Position für das Kreuz gebracht
pencolor("white")
fillcolor("white")
begin_fill()
penup()
right(90)
forward(l3)
pendown()


#Das Kreuz wird gezeichnet
right(90)
forward(l4)
left(90)
forward(l3)
right(90)
forward(l3)
left(90)
forward(l3)
left(90)
forward(l3)
right(90)
forward(l3)
left(90)
forward(l3)
left(90)
forward(l3)
right(90)
forward(l3)
left(90)
forward(l3)
left(90)
forward(l3)
right(90)
forward(l3)
left(90)
forward(l4)
end_fill()


exitonclick()