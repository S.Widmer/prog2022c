



from turtle import *
shape("turtle")

Seite = numinput("Eingabefenster", "Bitte die Seitenlänge der Dreiecke angeben:")
Seite2 = numinput("Eingabefenster", "Bitte die Seitenlänge der Vierecke angeben:")

penup()
right(60)
forward(Seite*3)
left(60)
pendown()

pensize(5)
pencolor("red")
fillcolor("pink")
begin_fill()



forward(Seite)
left(120)
forward(Seite)
left(120)
forward(Seite)

Seite=Seite*2

pencolor("yellow")
forward(Seite)
left(120)
forward(Seite)
left(120)
forward(Seite)

Seite=Seite*2

pencolor("blue")
forward(Seite)
left(120)
forward(Seite)
left(120)
forward(Seite)
left(120)

end_fill()
Seite=Seite*2
penup()
forward(Seite)
left(200)
pendown()
fillcolor("yellow")
begin_fill()

forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(30)

end_fill()
fillcolor("green")
begin_fill()
Seite2=Seite2/2

forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(105)

end_fill()
fillcolor("blue")
begin_fill()
Seite2=Seite2/2

forward(Seite2)
right(90)
forward(Seite2)
right(90)
forward(Seite2)
right(90)
forward(Seite2)
left(195)

end_fill()
fillcolor("pink")
begin_fill()
Seite2=Seite2/2

forward(Seite2)
right(90)
forward(Seite2)
right(90)
forward(Seite2)
right(90)
forward(Seite2)
left(105)

end_fill()
fillcolor("violet")
begin_fill()
Seite2=Seite2/2

forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)

end_fill()
exitonclick()


