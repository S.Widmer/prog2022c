from turtle import*
#Eingabeaufforderung für Seitenlänge
laenge = numinput("Dreieck", "Bitte Seitenlänge angeben")
#Strichdicke
pensize(2)
pencolor("red")
#grünes Dreieck
left(30)
forward(laenge)
fillcolor("green")
begin_fill()
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

#cyandreieck
right(120)
fillcolor("cyan")
begin_fill()
laenge = laenge *2
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()

#gelbes dreieck
right(120)
fillcolor("yellow")
begin_fill()
laenge = laenge *2
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()