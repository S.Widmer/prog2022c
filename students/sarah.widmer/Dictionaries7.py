
eingabetext = "David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen."
blacklist = ["david", "mayer", "htw", "chur"]
ausgabe = ""

klein = eingabetext.lower()
worte = klein.split()

for wort in worte:
    if wort not in blacklist:
        ausgabe = ausgabe + wort + " "
    elif wort in blacklist:
        ausgabe = ausgabe + len(wort)*"*" + " "

print(ausgabe)