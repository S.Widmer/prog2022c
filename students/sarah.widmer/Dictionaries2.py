def word_stat(text):
    dictionary = {}
    klein = text.lower()
    worte = klein.split()

    for eintrag in worte:
        if eintrag in dictionary:
            dictionary[eintrag] = dictionary[eintrag] + 1
        else:
            dictionary[eintrag] = 1
    return dictionary



l_input = "Der Tag begann sehr gut! Der Morgen war schön."

print(word_stat(l_input))

