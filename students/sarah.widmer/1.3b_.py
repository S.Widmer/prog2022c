from turtle import *

#b)

pensize(5)
pencolor("red")
fillcolor("cyan")
begin_fill()
right(54)
#right(54),da: blaues Quadrat steht gerade da -> die eine Linie exakt bei 180 Grad ist, wenn der
# Kreisanfang beim ursprünglichen Anfang der Turtle ist
#also 180 - 72 - 72 - 72 + 90 = 54Grad ((72 wird unten im zweiten Kommentar erklärt))
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(18)
#left(18), da 360grad / 5 Quadrate = 72Grad -> Anfangsstriche der einzelnen Quadrate
# müssen jeweils 72Grad voneinander entfernt sein
#90Grad (von der Ecke des Quadrates) - 72 Grad = 18Grad
end_fill()

fillcolor("yellow")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(18)
end_fill()

fillcolor("purple")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(18)
end_fill()

fillcolor("blue")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(18)
end_fill()


fillcolor("green")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(18)
end_fill()