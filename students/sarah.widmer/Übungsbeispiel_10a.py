def berechne_herzfrequenz(Alter):
    print("Die Herzfrequenz ist:", 220-Alter)

def get_billet_preis(Alter, Entfernung):
    if Alter < 6:
        print("Preis:", 0)
    elif Alter < 16:
        print("Preis:", (2 + Entfernung*0.25)/2)
    else:
        print ("Preis:", 2 + Entfernung*0.25)

while True:
    print("""==================================================
Programmübersicht:
      1 ... Preis für eine Fahrkarte berechnen
      2 ... Eintritt fur das Schwimmbad berechnen
      3 ... Mahngebuhr fur die Bibliothek berechnen
      
      0 ... Programm beenden
==================================================""")
    eingabe = int(input("Gewählte Option: "))
    if eingabe == 1:
        berechne_herzfrequenz(int(input("Bitte geben Sie ihr Alter ein: ")))
    elif eingabe == 2:
        get_billet_preis(int(input("Bitte geben Sie ihr Alter ein: ")), int(input("Wie viele km wollen Sie reisen? ")))
    elif eingabe == 0:
        break
    else:
        print("Ungültige Option!")