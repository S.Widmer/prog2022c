from turtle import *

#zuerst wird die turtle versteckt, sowie die Geschwindigkeit erhöht
hideturtle()
speed(5)

#dann wird das 400x400 grosse rote Rechteck gezeichnet
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
end_fill()

# nun wird der Stift zum weissen Kreuz gebracht
forward(165)
left(90)
forward(165)
right(90)

# jetzt wird das Kreuz gezeichnet
# die vier Balken sind mit folgener Funktion dargestellt
def kreuz():
    pencolor("white")
    fillcolor("white")
    begin_fill()
    right(90)
    forward(90)
    left(90)
    forward(70)
    left(90)
    forward(90)
    end_fill()

kreuz()
kreuz()
kreuz()
kreuz()

#nun bleibt nur noch das Quadrat in der Mitte des Kreuzes übrig
begin_fill()
forward(70)
left(90)
forward(70)
left(90)
forward(70)
left(90)
forward(70)
end_fill()