berge = {"Mt. Everest":8848, "Mount Godwin Austen":8610, "Lhotse":8516}

i=1
for eintrag in sorted(berge):
    print(str(i) + ". Berg:", eintrag)
    print(str(i) + ". Höhe:", berge[eintrag])
    print()
    i=i+1
    
for eintrag in sorted(berge):
    print(eintrag, "ist", berge[eintrag], "m (" + str(berge[eintrag]*3.28), "ft) hoch.")
