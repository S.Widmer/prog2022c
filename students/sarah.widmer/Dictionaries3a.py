
alphabet = {"A":"Alfa", "B":"Bravo", "C":"Charlie", "D":"Delta",
            "E":"Echo", "F":"Foxtrot", "G":"Golf", "H":"Hotel",
            "I":"India", "J":"Juliett", "K":"Kilo", "L":"Lima",
            "M":"Mike", "N":"November", "O":"Oscar", "P":"Papa",
            "Q":"Quebec", "R":"Romeo", "S":"Sierra", "T":"Tango",
            "U":"Uniform", "V":"Victor", "W":"Whiskey",
            "X":"X-ray", "Y":"Yankee", "Z":"Zulu"}

Ausgabe = ""
x = ""
eingabe=input("Welches Wort soll ich buchstabieren: ")
eingabe = eingabe.upper()
for buchstabe in eingabe:
    if buchstabe not in alphabet:
        print("Kann", eingabe, "nicht buchstabieren, da ", buchstabe, "nicht definiert wurde.")
        x = "no"
        break
    else:
        Ausgabe = Ausgabe + alphabet[buchstabe]  + " - "

if x != "no":
    print("Ausgabe:", Ausgabe[:-3])
