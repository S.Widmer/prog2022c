from turtle import *

#zuerst wird die turtle versteckt, sowie die Geschwindigkeit erhöht
hideturtle()
speed(10)

#dann wird das 400x400 grosse rote Rechteck gezeichnet
pencolor("red")
fillcolor("red")
begin_fill()

right(135)
forward(pow(400*400+400*400, 0.5)/2) #diese drei Zeilen, dienen dafür, dass man später home() verwenden kann um die 
left(135)                            # Mitte des Kreuzes zu finden

forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
end_fill()

# nun wird der Stift zum weissen Kreuz gebracht
home()


#um die Seitenlänge variabel zu machen, braucht es einen numinput. Dieser wird in eine Variabel gesetzt, damit man ihn
# mehr als einmal verwenden kann
laenge = numinput("Abfragedialog", "Bitte geben Sie eine Seitenlänge an.")

# jetzt wird das Kreuz gezeichnet
# die vier Balken sind mit folgener Funktion dargestellt
def kreuz():
    pencolor("white")
    fillcolor("white")
    begin_fill()
    right(90)
    forward(laenge)
    left(90)
    forward(laenge/1.5)
    left(90)
    forward(laenge)
    end_fill()

#durch home() wurde die Turtle in die Mitte des roten Quadrates geführt,
# da mein Kreuz jedoch nicht dort beginnt, kommt es mit den folgenden Zeilen dorthin
left(180) 
forward(laenge/3)
left(90)
forward(laenge/3)
left(90)

kreuz()
kreuz()
kreuz()
kreuz()

#nun bleibt nur noch das Quadrat in der Mitte des Kreuzes übrig
begin_fill()
forward(laenge/1.5)
left(90)
forward(laenge/1.5)
left(90)
forward(laenge/1.5)
left(90)
forward(laenge/1.5)
end_fill()