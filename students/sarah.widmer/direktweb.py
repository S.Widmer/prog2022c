from urllib.request import urlopen
from inscriptis import get_text

with urlopen('https://www.gutenberg.org/cache/epub/6079/pg6079.txt') as source:
    web_content = source.read().decode('utf8')
    text_content = get_text(web_content)
    print(text_content)

