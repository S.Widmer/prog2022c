from json import loads, dumps

einkaufsliste = []

try:
    with open("einkaufsliste.json", encoding="utf8") as f:
        json_string = f.read()
        einkaufsliste = loads(json_string)
except FileNotFoundError:
    print("Die Datei exisitert noch nicht, nur das was jetzt eingegeben wird, wird angezeigt")

while True:
    eingabe = int(input("""
        (1) artikel hinzufügen
        (2) artikel löschen
        (3) artikel suchen
        (4) einkaufsliste leeren
        (5) einkaufsliste im csv format exportieren
        (6) gesamtbetrag einkauf berechnen
        (0) exit
          """))
    
    if eingabe == 0:
        break

    if eingabe == 1:
        artikel = input("Artikel: ")
        preis = input("Preis: ")
        menge = input("Menge: ")
        artikelliste = []
        artikelliste.append(artikel)
        artikelliste.append(preis)
        artikelliste.append(menge)
        einkaufsliste.append(artikelliste)
        
    if eingabe == 2:
        löschen = input("Artikel: ")
        for artikel in einkaufsliste:
            if löschen in artikel:
                einkaufsliste.remove(artikel)

    if eingabe == 3:
        suche = input("Suchbegriff: ")
        for artikel in einkaufsliste:
            if suche in artikel[0]:
                print(artikel[0])
    
    if eingabe == 4:
        einkaufsliste = []
    
    if eingabe == 5:
        from csv import writer
        with open("einkaufsliste.csv", "w", encoding="utf8") as f:
            csv_writer = writer(f, delimiter=";")
            for artikel in einkaufsliste:
                csv_writer.writerow(artikel)
    
    if eingabe == 6:
        gesamtbetrag = 0
        for artikel in einkaufsliste:
            gesamtbetrag = gesamtbetrag + float(artikel[1])*int(artikel[2])
        print("Der Gesamtbetrag ist", gesamtbetrag, " CHF.")


with open("einkaufsliste.json", "w", encoding="utf8") as f:
    json_string = dumps(einkaufsliste)
    f.write(json_string)