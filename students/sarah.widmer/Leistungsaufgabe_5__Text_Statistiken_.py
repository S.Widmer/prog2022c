from urllib.request import urlopen
from csv import writer

ressource = input("Ressource: ")
 
if "https" in ressource:
    with urlopen(ressource) as source:
        content = source.read().decode("utf8")
else:
    with open(ressource, encoding="utf8") as f:
        content = f.read()


worte = content.split()
wörter = {}
buchstaben = {}

for wort in worte:
    if wort in wörter:
        wörter[wort] = wörter[wort] + 1
    else:
        wörter[wort] = 1
    for buchstabe in wort:
        if buchstabe in buchstaben:
            buchstaben[buchstabe] = buchstaben[buchstabe] + 1
        else:
            buchstaben[buchstabe] = 1

with open("wort.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for eintrag in wörter:
        csv_writer.writerow([eintrag, wörter[eintrag]])
        
with open("buchstabe.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for eintrag in buchstaben:
        csv_writer.writerow([eintrag, buchstaben[eintrag]])