
woerterbuch = {"Haus":"house", "Zahnarzt":"dentist", "Maus":"mouse", "Flasche":"bottle", "Lampe":"lamp", "Ei":"egg", "Schuh":"shoe", "Welt":"world", "Spiegel":"mirror", "Tee":"tea"}
i = 0

for eintrag in woerterbuch:
    abfrage = input("Was heisst " + eintrag + "? ")
    if abfrage == woerterbuch[eintrag]:
        print("Richtig!")
        i = i+1
    else:
        print("Falsch!")

print()
print("Sie haben", i, "von 10 Vokabeln richtig beantwortet!")