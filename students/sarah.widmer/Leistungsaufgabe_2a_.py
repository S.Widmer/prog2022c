# a)
i = 1
summe = 0
while True:
    eingabe = input("Geben Sie den " + str(i) + ". Preis ein: ")
    if eingabe == "x":
        if summe >= 100 and summe < 1000:
            print("5% Rabatt, Gesamtpreis =", summe*0.95, "CHF")
        elif summe >= 1000:
            print("10% Rabatt, Gesamtpreis =", summe*0.9, "CHF")
        elif summe < 100:
            print("Kein Rabatt, Gesamtpreis =", summe, "CHF")
        break
    i = i+1
    summe = summe + int(eingabe)
