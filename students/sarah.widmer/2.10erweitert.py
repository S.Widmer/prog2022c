from turtle import *

def formen():
    formen = textinput("Formen", "Welche Formen hätten Sie gerne?")
    for zeichen in formen:
        if zeichen == "d":
            forward (50)
            left(120)
            forward (50)
            left(120)
            forward (50)
            left(120)
            forward(50)
        elif zeichen == "#":
            forward (50)
            left(90)
            forward (50)
            left(90)
            forward (50)
            left(90)
            forward(50)
            left(90)
            forward(50)
        elif zeichen == "o":
            forward(25)
            circle(25)
            forward(25)
            
formen()