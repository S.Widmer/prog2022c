age = 5

if age > 17:
    legal_status = "volljährig"
elif age > 13:
    legal_status = "mündig minderjährig"
elif age > 5:
    legal_status = "unmündig"
elif age >= 0:
    legal_status = "geschäftsunfähig"
else:
    legal_status = "ungeboren"

print("Mit", age, "ist man", legal_status + ".")