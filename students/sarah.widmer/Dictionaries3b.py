alphabet = {"A":"Alfa", "B":"Bravo", "C":"Charlie", "D":"Delta",
            "E":"Echo", "F":"Foxtrot", "G":"Golf", "H":"Hotel",
            "I":"India", "J":"Juliett", "K":"Kilo", "L":"Lima",
            "M":"Mike", "N":"November", "O":"Oscar", "P":"Papa",
            "Q":"Quebec", "R":"Romeo", "S":"Sierra", "T":"Tango",
            "U":"Uniform", "V":"Victor", "W":"Whiskey",
            "X":"X-ray", "Y":"Yankee", "Z":"Zulu"}

Ausgabe = ""
eingabe=input("Welches Wort soll ich buchstabieren: ")
eingabe = eingabe.upper()
for buchstabe in eingabe:
    if buchstabe == "Ö":
        Ausgabe = Ausgabe + "Oscar - Echo - "
    elif buchstabe == "Ü":
        Ausgabe = Ausgabe + "Uniform - Echo - "
    elif buchstabe == "Ä":
        Ausgabe = Ausgabe + "Alfa - Echo - "
    else:
        Ausgabe = Ausgabe + alphabet[buchstabe]  + " - "


print("Ausgabe:", Ausgabe[:-3])

