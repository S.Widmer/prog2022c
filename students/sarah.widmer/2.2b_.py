from turtle import *

def dreieck(seitenlaenge, fuellfarbe):
    pencolor("red")
    fillcolor(fuellfarbe)
    begin_fill()
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()

right(90)
dreieck(numinput("Abfragedialog",
"Bitte geben Sie die Seitenlänge an"), "cyan")
dreieck(numinput("Abfragedialog",
"Bitte geben Sie die Seitenlänge an"), "yellow")
dreieck(numinput("Abfragedialog",
"Bitte geben Sie die Seitenlänge an"), "green")
