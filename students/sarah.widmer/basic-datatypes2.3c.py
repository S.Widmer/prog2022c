preisliste = {"Brot":3.2, "Milch":1.9, "Orangen":3.75, "Tomaten":2.2, "Tee":4.2, "Peanuts":3.9, "Ketchup":2.1}
dictionary = {}

for lebensmittel in preisliste:
    if "en" not in lebensmittel:
        dictionary[lebensmittel] = preisliste[lebensmittel]

print(dictionary)