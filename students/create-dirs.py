#!/usr/bin/env python

from pathlib import Path
print("Bitte die E-Mail Adressen der Kursteilnehmer(inn|en) pasten\n")
print("Abschluss mit 'complete'")

while student := input():
    # Zeilen ohne E-Mail Adresse überspringen
    if '@' not in student:
        continue
    elif student.strip() == 'complete':
        break

    student_dir = Path(student.split('@')[0])

    # Verzeichnis mit zugehörigem .gitkeep file anlegen
    if not student_dir.exists():
        student_dir.mkdir()
        student_dir.joinpath('.gitkeep').touch()
